CREATE TABLE "жанры"
(
    id SERIAL PRIMARY KEY,
    жанр Varchar(50) NOT NULL

);

CREATE TABLE "пользователь"
(
    id SERIAL PRIMARY KEY,
    имя Varchar(100) NOT NULL,
    фамилия Varchar(100) NOT NULL,
    google_auth Varchar(100) NOT NULL
);

CREATE TABLE "списки"
(
    id SERIAL PRIMARY KEY,
    статус_книги Varchar(50) NOT NULL
);


CREATE TABLE "завершённость"
(
    id SERIAL PRIMARY KEY,
    завершённость Varchar(50) NOT NULL
);

CREATE TABLE "главы"
(
    id SERIAL PRIMARY KEY,
    название Varchar(50) NOT NULL,
    номер INTEGER NOT NULL,
    текст Varchar(65000) NOT NULL
);

CREATE TABLE "обложка"
(
    id SERIAL PRIMARY KEY,
    данные bytea NOT NULL
);

CREATE TABLE "книги"
(
    id SERIAL PRIMARY KEY,
    название Varchar(50) NOT NULL,
    автор INTEGER REFERENCES "пользователь"(id),
    аннотация Varchar(1000) NOT NULL,
    жанр INTEGER REFERENCES "жанры"(id),
    завершённость INTEGER REFERENCES "завершённость"(id),
    просмотры INTEGER,
    дата_обновления DATE NOT NULL,
    обложка INTEGER REFERENCES "обложка"(id),
    черновик bool
);

CREATE TABLE "библиотека"
(
    пользователь INTEGER REFERENCES "пользователь"(id),
    книга INTEGER REFERENCES "книги"(id),
    статус INTEGER REFERENCES "списки"(id)
);

CREATE TABLE "последняя_глава"
(
    пользователь INTEGER REFERENCES "пользователь"(id),
    глава INTEGER REFERENCES "главы"(id),
	PRIMARY KEY (пользователь, глава)
);

CREATE TABLE "последние_книги"
(
    пользователь INTEGER REFERENCES "пользователь"(id),
    книга INTEGER REFERENCES "книги"(id),
	приоритет INTEGER
);


/*drop table "обложка";

drop table "последняя_глава";

drop table "главы";

drop table "библиотека";

drop table "списки";

drop table "книги";

drop table "жанры";

drop table "пользователь";

drop table "завершённость";*/

