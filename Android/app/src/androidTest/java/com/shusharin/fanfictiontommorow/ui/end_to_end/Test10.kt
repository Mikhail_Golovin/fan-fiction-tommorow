package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.CreateAndEditFragment
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test10 : BaseAuth() {
    @Test
    fun deletingBookChapter() {
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.works)
        scrollTo(R.id.list, 0)
        clickOnItemInRecycleView(R.id.list, R.id.nameBook, 0)
        clickOnView(R.id.change)

        scrollTo(R.id.list_chapter, 0)
        clickOnItemInRecycleView(R.id.list_chapter, R.id.delete, 0)
        clickOnTextInViewInDialog(R.string.dialog_yes)
        clickOnView(R.id.add_chapter)
        CreateAndEditFragment.edit.text.append("Название1")
        clickOnTextInViewInDialog(R.string.dialog_add)
        clickOnView(R.id.create)
    }
}