package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test18 :BaseAuth(){
    @Test
    fun filterInTheLibrary(){
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.library)
        clickOnView(R.id.app_bar_filter)

        clickOnView(R.id.name_status_reading)
        clickOnTextInViewInDialog(StatusReadingBook.READ.nameSRB)
        clickOnTextInViewInDialog(R.string.dialog_ok)


        clickOnView(R.id.apply_filters)
    }
}