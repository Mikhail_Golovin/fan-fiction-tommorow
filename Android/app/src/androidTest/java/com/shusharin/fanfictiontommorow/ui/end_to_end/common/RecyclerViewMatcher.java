package com.shusharin.fanfictiontommorow.ui.end_to_end.common;

import android.content.res.Resources;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

public class RecyclerViewMatcher {
    private final int recyclerViewId;
    public static View findViewLast;



    public RecyclerViewMatcher(int recyclerViewId) {
        this.recyclerViewId = recyclerViewId;
    }

    public Matcher<View> atPosition(final int position) {
        return atPositionOnView(position, -1);
    }
    public static View getFindViewLast() {
        return findViewLast;
    }
    public Matcher<View> atPositionOnView(final int position, final int targetViewId) {

        return new TypeSafeMatcher<>() {
            Resources resources = null;
            View childView;

            public void describeTo(Description description) {
                String idDescription = Integer.toString(recyclerViewId);
                if (this.resources != null) {
                    try {
                        idDescription = this.resources.getResourceName(recyclerViewId);
                    } catch (Resources.NotFoundException var4) {
                        idDescription = String.format("%s (resource name not found)",
                                recyclerViewId);
                    }
                }

                description.appendText("with id: " + idDescription);
            }

            public boolean matchesSafely(View view) {

                this.resources = view.getResources();

                if (childView == null) {
                    RecyclerView recyclerView =
                            view.getRootView().findViewById(recyclerViewId);
                    if (recyclerView != null && recyclerView.getId() == recyclerViewId) {
                        try {
                            childView = Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(position)).itemView;
                        }catch (Exception e){
                            childView=null;
                            return false;
                        }
                    } else {
                        return false;
                    }
                }

                if (targetViewId == -1) {
                    if ( view == childView) {
                        findViewLast = childView;
                    }
                    return view == childView;
                } else {
                    View targetView = childView.findViewById(targetViewId);
                    if ( view == targetView) {
                        findViewLast = targetView;
                    }
                    return view == targetView;
                }

            }
        };
    }
}