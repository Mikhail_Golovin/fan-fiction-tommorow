package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.NOT_AUTH
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test19 : Base() {
    @Test
    fun logoutSuccess() {
        login()
        logout()
        assertEquals(NOT_AUTH, MainActivity.idUser)
    }
}