package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test7 : Base() {
    @Test
    fun goToTheAuthorProfile() {
        clickOnView(R.id.nav_my_profile)
        clickOnTextInViewInDialog(R.string.dialog_ok)
        openBookInPopulate()
        clickOnView(R.id.lists)
        clickOnTextInViewInDialog(R.string.dialog_ok)
        clickOnView(R.id.nameAuthor)
    }
}