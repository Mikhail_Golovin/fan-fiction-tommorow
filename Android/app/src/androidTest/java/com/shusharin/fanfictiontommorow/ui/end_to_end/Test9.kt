package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test9 : BaseAuth() {
    @Test
    fun changingTheTextOfTheChapter() {
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.works)
        scrollTo(R.id.list,0)
        clickOnItemInRecycleView(R.id.list, -1, 0)
        clickOnView(R.id.change)

        scrollTo(R.id.list_chapter,0)
        clickOnItemInRecycleView(R.id.list_chapter, -1, 0)
        setText(R.id.edit_text, "это текст главы")
        clickOnView(R.id.save)
        clickOnView(R.id.create)
    }
}