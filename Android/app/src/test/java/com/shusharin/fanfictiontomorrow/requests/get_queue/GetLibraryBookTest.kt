package com.shusharin.fanfictiontomorrow.requests.get_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.LibraryBook
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetLibraryBookTest : RequestTestBase() {
    private lateinit var mockRequest: GetLibraryBook

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getLibraryBook)
    }

    @Test
    fun sendRequestFake() {
        val libraryBook = LibraryBook("Название", 1, 1, 1, false, 1, 1)
        val libraryBookParams = LibraryBookParams(1, 1)
        sendRequest(mockRequest, libraryBookParams, libraryBook)

        while (quantityActiveRequests != 0) {
            Thread.sleep(10)
        }
        val readBook = slotReadBook.captured

        assert(readBook.id == libraryBookParams.idBook)
        assert(readBook.name == libraryBook.name)
        assert(!readBook.isDraft)
        assert(readBook.genre.id == libraryBook.genreId)
        assert(readBook.idCoverBook == libraryBook.coverId)
        assert(readBook.completeness.id == libraryBook.completenessId)
        assert(readBook.statusReadingBook.id == libraryBook.statusId)
    }

    @Test
    fun sendRequestFakeStatusReadingLess() {
        val libraryBook = LibraryBook("Название", 1, 1, -10, false, 1, 1)
        val libraryBookParams = LibraryBookParams(1, 1)
        sendRequest(mockRequest, libraryBookParams, libraryBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val readBook = slotReadBook.captured

        assert(readBook.id == libraryBookParams.idBook)
        assert(readBook.name == libraryBook.name)
        assert(!readBook.isDraft)
        assert(readBook.genre.id == libraryBook.genreId)
        assert(readBook.idCoverBook == libraryBook.coverId)
        assert(readBook.completeness.id == libraryBook.completenessId)
        assert(readBook.statusReadingBook.id == 5)
    }

    @Test
    fun sendRequestFakeStatusReadingMore() {
        val libraryBook = LibraryBook("Название", 1, 1, 10, false, 1, 1)
        val libraryBookParams = LibraryBookParams(1, 1)
        sendRequest(mockRequest, libraryBookParams, libraryBook)
        while (quantityActiveRequests != 0) {
            Thread.sleep(10)
        }
        val readBook = slotReadBook.captured

        assert(readBook.id == libraryBookParams.idBook)
        assert(readBook.name == libraryBook.name)
        assert(!readBook.isDraft)
        assert(readBook.genre.id == libraryBook.genreId)
        assert(readBook.idCoverBook == libraryBook.coverId)
        assert(readBook.completeness.id == libraryBook.completenessId)
        assert(readBook.statusReadingBook.id == 5)
    }
}