package com.shusharin.fanfictiontomorrow.requests.add_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class AddBookTest : RequestTestBase() {
    private lateinit var mockRequest: AddBook

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::addBook)
    }

    @Test
    fun sendRequestFake() {
        val idBook = 1
        val bookParams = mockk<BookParams>()
        sendRequest(mockRequest, bookParams, idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val idBookResponse = slotInt.captured
        assert(idBookResponse == idBook)
    }
}