package com.shusharin.fanfictiontomorrow.requests.change

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class ChangeCoverTest : RequestTestBase() {
    private lateinit var mockRequest: ChangeCover

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::changeCover)
    }

    @Test
    fun sendRequestFake() {
        val result = true
        val changeCoverParams = mockk<ChangeCoverParams>()
        sendRequest(mockRequest, changeCoverParams, result)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val resultResponse = slotBoolean.captured
        assert(resultResponse == result)
    }
}