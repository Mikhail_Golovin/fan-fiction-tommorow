package com.shusharin.fanfictiontomorrow.ui.main_page

import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.ui.main_page.adapters.AdapterSection
import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class MainPageViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    val sections = HashMap<Sections, HashMap<Int, FindBook>>()
    val getListIdBooksInSection = api.getListIdBooksInSection()
    val liveSearchInSectionBook = getListIdBooksInSection.liveSearchInSectionBook

    val getListIdBooksInSection1 = api.getListIdBooksInSection()
    val liveSearchInSectionBook1 = getListIdBooksInSection1.liveSearchInSectionBook

    val getListIdLastBooks = api.getListIdLastBooks()
    val liveLastBooks = getListIdLastBooks.liveLastBooks
}