package com.shusharin.fanfictiontomorrow.ui.edit_chapter

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class EditChapterViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    var chapter: Chapter = Chapter()
    var idChapter: Int = -1

    var isChangeChapter = false

    val getChapter = api.getChapter()
    val liveChapter = getChapter.liveChapter
}