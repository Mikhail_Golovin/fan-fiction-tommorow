package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListChapterInBook
import retrofit2.Call

class ListIdChaptersParams(val idBook: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdChaptersParams(idBook=$idBook)"
    }
}

class GetListIdChapters(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : MyRequest<ListChapterInBook, ListIdChaptersParams, HashMap<Int, Chapter>>(
    startRefresh,
    stopRefresh
) {
    private val chapters = HashMap<Int, Chapter>()
    val liveChapters: MutableLiveData<HashMap<Int, Chapter>>
        get() = liveData

    fun sendRequest(idBook: Int) {
        if (idBook != -1) {
            sendRequest(ListIdChaptersParams(idBook))
        }
    }

    override fun analysisOfResponseBody(
        responseBody: ListChapterInBook,
        params: ListIdChaptersParams,
    ) {
        for (chapter in responseBody.chapters) {
            setChapter(Chapter(chapter))
        }
        setLiveData(chapters)
    }

    fun setChapter(chapter: Chapter) {
        chapters[chapter.id] = chapter
    }

    override fun call(params: ListIdChaptersParams): Call<ListChapterInBook> {
        return service.getChapters(params.idBook)
    }
}