package com.shusharin.fanfictiontomorrow.requests

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

open class ChaptersRequests(
    override val owner: LifecycleOwner, override val api: MyApiServices,
) : MyRequestIdentity<Boolean, Int>({ }, { }),
    IDeleteChapter {
    protected open val liveIsSuccess: MutableLiveData<Boolean>
        get() = liveData

    inner class AddChaptersRequest(
        override val owner: LifecycleOwner,
        override val api: MyApiServices,
    ) : IAddChapter {
        inner class ChangeChaptersRequest(
            override val owner: LifecycleOwner,
            override val api: MyApiServices,
        ) : IChangeChapter {

            fun sendRequest(
                changesChapters: java.util.HashMap<Int, Chapter>,
            ) {
                changeChapter(changesChapters)
            }

            override fun completedAddChapters() {
                setLiveData(true)
            }

        }

        fun sendRequest(
            idBook: Int,
            addedChapters: java.util.HashMap<Int, Chapter>,
            changesChapters: java.util.HashMap<Int, Chapter>,
        ) {
            addChapter(idBook, addedChapters, changesChapters)
        }

        override fun completedAddChapters(
            changesChapters: java.util.HashMap<Int, Chapter>,
        ) {
            val addChapters = ChangeChaptersRequest(owner, api)
            addChapters.sendRequest(changesChapters)
        }
    }

    override fun completedDeleteChapters(
        idBook: Int,
        addedChapters: java.util.HashMap<Int, Chapter>,
        changesChapters: java.util.HashMap<Int, Chapter>,
    ) {
        val addChapters = AddChaptersRequest(owner, api)
        addChapters.sendRequest(idBook, addedChapters, changesChapters)
    }

    override fun call(params: Int): Call<Boolean> {
        return service.deleteChapter(-1)
    }
}