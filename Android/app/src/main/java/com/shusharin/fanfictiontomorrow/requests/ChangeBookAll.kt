package com.shusharin.fanfictiontomorrow.requests

import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class ChangeBookAll(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
) : ChaptersRequests(owner, api),
    IChangeCover,
    IChangeBook {
    var idBook = -1
    val liveIsChanged: MutableLiveData<Boolean>
        get() = liveIsSuccess

    fun sendRequest(
        bitmap: Bitmap,
        idBookNew: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        idBook = idBookNew
        changeCover(bitmap, idBookNew, bookClass, deleteChapters, addedChapters, changesChapters)
    }

    override fun setIsSuccessChangeCover(
        it: Boolean,
        idBookNew: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        changeBook(idBookNew, bookClass, deleteChapters, addedChapters, changesChapters)
    }

    override fun setIdBook(
        idBook: Int,
        deleteChapters: java.util.HashMap<Int, Chapter>,
        addedChapters: java.util.HashMap<Int, Chapter>,
        changesChapters: java.util.HashMap<Int, Chapter>,
    ) {
        deleteChapter(idBook, deleteChapters, addedChapters, changesChapters)
    }
}