package com.shusharin.fanfictiontomorrow.requests.get_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import retrofit2.Call

class AuthUserIdParams(
    val firstName: String,
    val lastName: String,
    val email: String,
) : MyRequestParams() {
    override fun toString(): String {
        return "AuthUserIdParams(firstName='$firstName', lastName='$lastName', email='$email')"
    }
}

class GetAuthUserId(
    startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) : MyRequestIdentity<Int, AuthUserIdParams>(startRefresh, stopRefresh) {
    val liveIdUser: MutableLiveData<Int>
        get() = liveData

    fun sendRequest(
        firstName: String,
        lastName: String,
        email: String,
    ) {
        sendRequest(
            AuthUserIdParams(
                firstName.filter { !it.isWhitespace() },
                lastName.filter { !it.isWhitespace() },
                email.filter { !it.isWhitespace() })
        )
    }

    override fun analysisOfResponseBody(responseBody: Int, params: AuthUserIdParams) {
        MainActivity.idUser = responseBody
        setLiveData(responseBody)
    }

    override fun call(params: AuthUserIdParams): Call<Int> {
        return service.getAuthUserId(params.firstName, params.lastName, params.email)
    }
}