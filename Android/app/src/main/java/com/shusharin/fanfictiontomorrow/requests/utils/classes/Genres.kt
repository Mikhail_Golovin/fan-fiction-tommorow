package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.shusharin.fanfictiontomorrow.R

enum class Genres(val id: Int, val idImage: Int, val nameG: Int) {
    SCIENCE_FICTION(1, R.drawable.sciency_fiction, R.string.genre_fantastic),
    FANTASY(2, R.drawable.fantasy, R.string.genre_fantasy),
    DETECTIVE(3, R.drawable.detective, R.string.genre_detective),
    ACTION_MOVIE(4, R.drawable.action_movie, R.string.genre_action_movie),
    ROMANCE(5, R.drawable.romance, R.string.genre_romance),
    THRILLER(6, R.drawable.thriller, R.string.genre_thriller),
    HUMOR(7, R.drawable.humor, R.string.genre_humor),
    HORRORS(8, R.drawable.horrors, R.string.genre_horrors),
    VARIOUS(9, R.drawable.various, R.string.genre_various);

    companion object {
        fun getGenre(idGenre: Int): Genres {
            return Genres.values()[idGenre - 1]
        }
    }
}