package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonProcessingException

import com.fasterxml.jackson.databind.ObjectMapper
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book

class BookClass {
    @JsonProperty
    var name: String = ""

    @JsonProperty
    var authorId = 0

    @JsonProperty
    var annotation: String = ""

    @JsonProperty
    var genreId = 0

    @JsonProperty
    var completenessId = 0

    @JsonProperty
    var date: String = ""

    @JsonProperty
    var coverId = 0

    @JsonProperty
    var views = 0

    @JsonProperty
    var isDraft = false

    constructor()

    constructor(
        name: String, authorId: Int, annotation: String,
        genreId: Int, completenessId: Int, views: Int, date: String, coverId: Int, isDraft: Boolean,
    ) {
        this.name = name
        this.authorId = authorId
        this.annotation = annotation
        this.genreId = genreId
        this.completenessId = completenessId
        this.date = date
        this.coverId = coverId
        this.isDraft = isDraft
        this.views = views

    }

    constructor(
        book: Book,
    ) {
        this.name = book.name
        this.authorId = book.author.id
        this.annotation = book.annotate
        this.genreId = book.genre.id
        this.completenessId = book.completeness.id
        this.date = book.date
        this.coverId = book.idCoverBook
        this.views = book.quantityOfViews.toInt()
        this.isDraft = book.isDraft
    }

    override fun toString(): String {
        try {
            return ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this)
        } catch (e: JsonProcessingException) {
            e.printStackTrace()
        }
        return ""
    }
}