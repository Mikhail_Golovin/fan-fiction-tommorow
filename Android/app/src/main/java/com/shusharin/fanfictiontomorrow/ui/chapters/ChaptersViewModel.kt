package com.shusharin.fanfictiontomorrow.ui.chapters

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class ChaptersViewModel(api: MyApiServices) : ViewModelUpdatable(api){
    val getLastChapter = api.getLastChapter()
    val liveIdLastChapter = getLastChapter.liveIdLastChapter

    val getListIdChapters = api.getListIdChapters()
    val liveChapters = getListIdChapters.liveChapters
}