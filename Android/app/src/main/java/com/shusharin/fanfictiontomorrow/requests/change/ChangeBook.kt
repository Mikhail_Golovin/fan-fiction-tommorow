package com.shusharin.fanfictiontomorrow.requests.change

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call

class ChangeBookParams(val idBook: Int, val bookClass: BookClass) : MyRequestParams() {
    override fun toString(): String {
        return "ChangeBookParams(idBook=$idBook, bookClass=$bookClass)"
    }
}

class ChangeBook(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,

    ) : MyRequestIdentity<Int, ChangeBookParams>(startRefresh, stopRefresh) {
    val idBook: MutableLiveData<Int>
        get() = liveData

    fun sendRequest(idBook: Int, bookClass: BookClass) {
        sendRequest(ChangeBookParams(idBook, bookClass))
    }

    override fun call(params: ChangeBookParams): Call<Int> {
        val requestBody = RequestBody.create(
            MediaType.get("application/json; charset=utf-8"), params.bookClass.toString()
        )
        return service.changeBook(params.idBook, requestBody)
    }

}