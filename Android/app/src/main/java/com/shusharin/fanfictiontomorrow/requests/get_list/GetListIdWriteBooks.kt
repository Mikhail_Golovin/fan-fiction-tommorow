package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetBookClass
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class ListIdWriteBooksParams(val idUser: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdWriteBooksParams(idUser=$idUser)"
    }
}

class GetListIdWriteBooks(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : GetListIdBook<ListIdWriteBooksParams, Book>(
    startRefresh,
    stopRefresh
),
    IGetBookClass {
    val liveWriteBooks: MutableLiveData<HashMap<Int, Book>>
        get() = liveData

    fun sendRequest(idUser: Int) {
        data.clear()
        sendRequest(ListIdWriteBooksParams(idUser))
    }

    override fun analysisOfResponseBody(responseBody: ListBookId, params: ListIdWriteBooksParams) {
        getGetBookClass(params.idUser, responseBody)
    }

    override fun call(params: ListIdWriteBooksParams): Call<ListBookId> {
        return service.getWriteBook(params.idUser)
    }

    override fun setBookClass(it: Book) {
        setLiveData(it, true)
    }
}