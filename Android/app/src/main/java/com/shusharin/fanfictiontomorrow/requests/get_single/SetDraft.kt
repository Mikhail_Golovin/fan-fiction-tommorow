package com.shusharin.fanfictiontomorrow.requests.get_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import retrofit2.Call

class SetDraftParams(val idBook: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "SetDraftParams(idBook=$idBook)"
    }
}

class SetDraft(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequestIdentity<Boolean, SetDraftParams>(startRefresh, stopRefresh) {
    val liveHasSetDraft: MutableLiveData<Boolean>
        get() = liveData

    fun sendRequest(idBook: Int): MutableLiveData<Boolean> {
        sendRequest(SetDraftParams(idBook))
        return liveHasSetDraft
    }

    override fun call(params: SetDraftParams): Call<Boolean> {
        return service.setDraft(params.idBook)
    }

}