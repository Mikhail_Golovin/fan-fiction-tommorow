package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.shusharin.fanfictiontomorrow.requests.utils.model.ChapterClass
import com.shusharin.fanfictiontomorrow.requests.utils.model.ChaptersInBook

class Chapter() {

    var id: Int = -1
    var name: String = ""
    var text: String = ""
    var numberInBook: Int = 0

    constructor(chapter: ChaptersInBook) : this() {
        id = chapter.chapterId
        name = chapter.name
        numberInBook = chapter.numberInBook
    }

    constructor(idChapter: Int, nameChapter: String) : this() {
        id = idChapter
        name = nameChapter
    }

    constructor(nameChapter: String, textChapter: String) : this() {
        text = textChapter
        name = nameChapter
    }

    constructor(idChapter: Int) : this() {
        id = idChapter
    }

    companion object {
        fun convertToArrayAndSort(chapters: HashMap<Int, Chapter>): Array<Pair<Int, Chapter>> {
            val temp = chapters.toList().toTypedArray()
            temp.sortBy { it.second.numberInBook }
            return temp
        }
    }

    constructor(idChapter: Int, text: String, numberInBook: Int) : this() {
        id = idChapter
        this.text = text
        this.numberInBook = numberInBook
    }

    constructor(chapterClass: ChapterClass) : this() {
        id = chapterClass.chapterId
        text = chapterClass.text
        numberInBook = chapterClass.chapterId
    }

    override fun toString(): String {
        return "Chapter(idChapter=$id, name='$name', text='$text', numberInBook=$numberInBook)"
    }
}