package com.shusharin.fanfictiontomorrow.utils

import android.content.res.Resources
import androidx.lifecycle.LifecycleOwner
import com.shusharin.fanfictiontomorrow.requests.add_queue.AddChapter
import com.shusharin.fanfictiontomorrow.requests.add_single.AddBook
import com.shusharin.fanfictiontomorrow.requests.add_single.AddBookInLibrary
import com.shusharin.fanfictiontomorrow.requests.add_single.AddCover
import com.shusharin.fanfictiontomorrow.requests.change.ChangeBook
import com.shusharin.fanfictiontomorrow.requests.change.ChangeChapter
import com.shusharin.fanfictiontomorrow.requests.change.ChangeCover
import com.shusharin.fanfictiontomorrow.requests.delete_queue.DeleteChapter
import com.shusharin.fanfictiontomorrow.requests.get_list.*
import com.shusharin.fanfictiontomorrow.requests.get_queue.*
import com.shusharin.fanfictiontomorrow.requests.get_single.*

class MyApiServices(
    val owner: LifecycleOwner, val resources: Resources,
    val startRefresh: () -> Unit, val stopRefresh: () -> Unit,
) {
    private val myApiServices: MyApiServices
        get() = this

    fun getAuthUserId(): GetAuthUserId {
        return GetAuthUserId(startRefresh, stopRefresh)
    }

    fun getChapter(): GetChapter {
        return GetChapter(startRefresh, stopRefresh)
    }

    fun deleteChapter(): DeleteChapter {
        return DeleteChapter(startRefresh, stopRefresh)
    }

    fun getLastChapter(): GetLastChapter {
        return GetLastChapter(startRefresh, stopRefresh)
    }

    fun changeCover(): ChangeCover {
        return ChangeCover(startRefresh, stopRefresh)
    }

    fun addCover(): AddCover {
        return AddCover(startRefresh, stopRefresh)
    }

    fun addBook(): AddBook {
        return AddBook(startRefresh, stopRefresh)
    }

    fun getUser(): GetUser {
        return GetUser(startRefresh, stopRefresh)
    }

    fun getListIdChapters(): GetListIdChapters {
        return GetListIdChapters(startRefresh, stopRefresh)
    }

    fun addBookInLibrary(): AddBookInLibrary {
        return AddBookInLibrary(startRefresh, stopRefresh)
    }

    fun addChapter(): AddChapter {
        return AddChapter(startRefresh, stopRefresh)
    }

    fun changeChapter(): ChangeChapter {
        return ChangeChapter(startRefresh, stopRefresh)
    }

    fun changeBook(): ChangeBook {
        return ChangeBook(startRefresh, stopRefresh)
    }

    fun getPresentBookInGenre(): GetPresentBookInGenre {
        return GetPresentBookInGenre(startRefresh, stopRefresh)
    }

    fun getCover(): GetCover {
        return GetCover(resources, startRefresh, stopRefresh)
    }

    fun getSearchBook(): GetSearchBook {
        return GetSearchBook(owner, myApiServices, resources, startRefresh, stopRefresh)
    }

    fun setDraft(): SetDraft {
        return SetDraft(startRefresh, stopRefresh)
    }

    fun getMainPageBook(): GetMainPageBook {
        return GetMainPageBook(owner, myApiServices, resources, startRefresh, stopRefresh)
    }

    fun getListIdBooksInSection(): GetListIdBooksInSection {
        return GetListIdBooksInSection(owner, myApiServices, startRefresh, stopRefresh)
    }

    fun getListIdSearchBooksInLibrary(): GetListIdSearchBooksInLibrary {
        return GetListIdSearchBooksInLibrary(owner, myApiServices, startRefresh, stopRefresh)
    }

    fun getListIdBooksLibrary(): GetListIdBooksLibrary {
        return GetListIdBooksLibrary(owner, myApiServices, startRefresh, stopRefresh)
    }

    fun getListIdGenreBooks(): GetListIdGenreBooks {
        return GetListIdGenreBooks(owner, myApiServices, startRefresh, stopRefresh)
    }

    fun getListIdSearchBooks(): GetListIdSearchBooks {
        return GetListIdSearchBooks(owner, myApiServices, startRefresh, stopRefresh)
    }

    fun getListIdLastBooks(): GetListIdLastBooks {
        return GetListIdLastBooks(owner, myApiServices, startRefresh, stopRefresh)
    }

    fun getLibraryBook(): GetLibraryBook {
        return GetLibraryBook(owner, myApiServices, resources, startRefresh, stopRefresh)
    }

    fun getBookClass(): GetBookClass {
        return GetBookClass(owner, myApiServices, resources, startRefresh, stopRefresh)
    }

    fun changeBookAll(): com.shusharin.fanfictiontomorrow.requests.ChangeBookAll {
        return com.shusharin.fanfictiontomorrow.requests.ChangeBookAll(owner, myApiServices)
    }

    fun addBookAll(): com.shusharin.fanfictiontomorrow.requests.AddBookAll {
        return com.shusharin.fanfictiontomorrow.requests.AddBookAll(owner, myApiServices)
    }

    fun getListIdWriteBooks(): GetListIdWriteBooks {
        return GetListIdWriteBooks(owner, myApiServices, startRefresh, stopRefresh)
    }


}