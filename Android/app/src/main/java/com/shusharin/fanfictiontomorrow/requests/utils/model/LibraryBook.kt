package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty

class LibraryBook {
    @JsonProperty
    var name: String? = ""

    @JsonProperty
    var authorId = 0

    @JsonProperty
    var coverId = 0

    @JsonProperty
    var isDraft = false

    @JsonProperty
    var statusId = 0

    @JsonProperty
    var genreId = 0

    @JsonProperty
    var completenessId = 0

    constructor()
    constructor(
        name: String,
        authorId: Int,
        coverId: Int,
        statusId: Int,
        isDraft: Boolean,
        genreId: Int,
        completenessId: Int,
    ) {
        this.name = name
        this.authorId = authorId
        this.coverId = coverId
        this.isDraft = isDraft
        this.statusId = statusId
        this.genreId = genreId
        this.completenessId = completenessId
    }
}