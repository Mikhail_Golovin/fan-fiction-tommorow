package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.fasterxml.jackson.annotation.JsonProperty

class Genre() {
    var genre = 0

    @JsonProperty("isPresent")
    var isPresent = false

    constructor(genre: Int, isPresent: Boolean) : this() {
        this.genre = genre
        this.isPresent = isPresent
    }

    override fun toString(): String {
        return "Genre(genre=$genre, isPresent=$isPresent)"
    }

}