package com.shusharin.fanfictiontomorrow.ui.read_book

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class MyReadBookViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    var text: String = ""
    var textPages = ArrayList<String>()
    lateinit var chapter: Chapter
    lateinit var listIterator: ListIterator<Pair<Int, Chapter>>
    var page = 0
    var maxPage = 0

    val getListIdChapters = api.getListIdChapters()
    val liveChapters = getListIdChapters.liveChapters

    val getChapter = api.getChapter()
    val liveChapter = getChapter.liveChapter
}