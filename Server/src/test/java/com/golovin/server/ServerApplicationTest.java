package com.golovin.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.golovin.server.Services.BookService;
import com.golovin.server.Services.ChaptersService;
import com.golovin.server.Services.CoverService;
import com.golovin.server.Services.LibraryService;
import com.golovin.server.classes.*;
import com.golovin.server.models.Books;
import com.golovin.server.models.Chapters;
import com.golovin.server.models.Cover;
import com.golovin.server.models.Library;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class ServerApplicationTest {


    private final ServerApplication serverApplication = new ServerApplication();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void getPresentBookInGenre() throws JsonProcessingException {
        String books = serverApplication.getPresentBookInGenre();
        ListPresentBookInGenre presentBookInGenre = objectMapper.readValue(books, ListPresentBookInGenre.class);
        assertTrue(presentBookInGenre.getGenres().get(0).getIsPresent());
        assertTrue(presentBookInGenre.getGenres().get(1).getIsPresent());
        assertTrue(presentBookInGenre.getGenres().get(2).getIsPresent());
        assertFalse(presentBookInGenre.getGenres().get(3).getIsPresent());
    }


    @Test
    void getLibraryBook() throws JsonProcessingException {
        String book = serverApplication.getLibraryBook(2, 3);
        LibraryBook libraryBook = objectMapper.readValue(book, LibraryBook.class);
        assertEquals(2, libraryBook.getAuthorId());
        assertEquals("Жизнь после второй смерти", libraryBook.getName());
        assertFalse(libraryBook.getIsDraft());
        assertEquals(3, libraryBook.getGenreId());
        assertEquals(3, libraryBook.getCoverId());
        assertEquals(2, libraryBook.getCompletenessId());
        assertEquals(3, libraryBook.getStatusId());
    }

    @Test
    void getBook() throws JsonProcessingException {
        String book = serverApplication.getBook(1, -1);
        BookClass bookClass = objectMapper.readValue(book, BookClass.class);
        assertEquals(1, bookClass.getAuthorId());
        assertEquals("В поисках кота", bookClass.getName());
        assertFalse(bookClass.getIsDraft());
        assertEquals(2, bookClass.getGenreId());
        assertEquals(1, bookClass.getCoverId());
        assertEquals(1, bookClass.getCompletenessId());
        assertEquals("Поиск кота поистине важен!", bookClass.getAnnotation());
    }

    @Test
    void getMainPageBook() throws JsonProcessingException {
        String book = serverApplication.getMainPageBook(1);
        MainPageBook mainPageBook = objectMapper.readValue(book, MainPageBook.class);
        assertEquals(1, mainPageBook.getAuthorId());
        assertEquals("В поисках кота", mainPageBook.getName());
        assertFalse(mainPageBook.getIsDraft());
        assertEquals(1, mainPageBook.getCoverId());
    }

    @Test
    void getSearchBook() throws JsonProcessingException {
        String book = serverApplication.getSearchBook(1, 2);
        SearchBook searchBook = objectMapper.readValue(book, SearchBook.class);
        assertEquals(1, searchBook.getAuthorId());
        assertEquals("В поисках кота", searchBook.getName());
        assertFalse(searchBook.getIsDraft());
        assertEquals(1, searchBook.getCoverId());
        assertEquals("Поиск кота поистине важен!", searchBook.getAnnotation());
        assertEquals(2, searchBook.getGenre());
    }

    @Test
    void getUser() throws JsonProcessingException {
        String user = serverApplication.getUser(1);
        UserClass userClass = objectMapper.readValue(user, UserClass.class);
        assertEquals("Петр", userClass.getName());
        assertEquals("Петров", userClass.getSurname());
    }

    @Test
    void getLibrary() throws JsonProcessingException {
        String library = serverApplication.getLibrary(2);
        ListBooksId booksId = objectMapper.readValue(library, ListBooksId.class);
        assertEquals(1, booksId.getBooksId().size());
        assertEquals(3, booksId.getBooksId().get(0));
    }

    @Test
    void getWriteBook() throws JsonProcessingException {
        String writeBook = serverApplication.getWriteBook(1);
        ListBooksId booksId = objectMapper.readValue(writeBook, ListBooksId.class);
        assertEquals(3, booksId.getBooksId().size());
        assertEquals(1, booksId.getBooksId().get(0));
    }

    @Test
    void getWithStatus() throws JsonProcessingException {
        String bookWithStatus = serverApplication.getWithStatus(3, 2);
        ListBooksId booksId = objectMapper.readValue(bookWithStatus, ListBooksId.class);
        assertEquals(1, booksId.getBooksId().size());
        assertEquals(3, booksId.getBooksId().get(0));
    }

    @Test
    void getGenreBook() throws JsonProcessingException {
        String genreBook = serverApplication.getGenreBook(3);
        ListBooksId booksId = objectMapper.readValue(genreBook, ListBooksId.class);
        assertEquals(1, booksId.getBooksId().size());
        assertEquals(3, booksId.getBooksId().get(0));
    }

    @Test
    void getChapters() throws JsonProcessingException {
        String chapter = serverApplication.getChapters(3);
        ListChapterInBook listChapterInBook = objectMapper.readValue(chapter, ListChapterInBook.class);
        assertEquals(1, listChapterInBook.getChapters().size());
        assertEquals(1, listChapterInBook.getChapters().get(0).getChapterId());
        assertEquals("Глава 1", listChapterInBook.getChapters().get(0).getName());
        assertEquals(0, listChapterInBook.getChapters().get(0).getNumberInBook());
    }

    @Test
    void getChapter() throws JsonProcessingException {
        String chapter = serverApplication.getChapter(1, 3);
        ChapterClass chapterClass = objectMapper.readValue(chapter, ChapterClass.class);
        assertEquals("Пролог. Возможно вы задавались себе вопросом: Почему у других есть кот, а у меня нет? Почему у других есть кот, а вас нет? Эта книга о том, что делать, если эти вопросы приходят вам в голову",
                chapterClass.getText());
        assertEquals(3, chapterClass.getChapterId());
    }

    @Test
    void getAuthUserId() {
        int userId = serverApplication.getAuthUserId("Петр", "Петров",
                "petr.petrov@gmail.com");
        assertEquals(userId, 1);
    }

    @Test
    void getBooksInSection() throws JsonProcessingException {
        String booksInSection = serverApplication.getBooksInSection(1);
        ListBooksId listBooksId = objectMapper.readValue(booksInSection, ListBooksId.class);
        BookService bookService = new BookService();
        List<Books> bookList = bookService.findAll();
        bookList.sort(Comparator.comparingInt(Books::getId));
        listBooksId.getBooksId().sort(Integer::compareTo);
        int size = bookList.size();
        assertEquals(size, listBooksId.getBooksId().size());
        assertEquals(bookList.get(0).getId(), listBooksId.getBooksId().get(0));
        assertEquals(bookList.get(bookList.size() - 1).getId(),
                listBooksId.getBooksId().get(listBooksId.getBooksId().size() - 1));
    }

    @Test
    void addBookInLibrary() {
        LibraryService libraryService = new LibraryService();
        int statusId = serverApplication.addBookInLibrary(4, 3, 3);
        assertEquals(3, statusId);
        List<Library> libraryList = libraryService.getBooksFromLibraryWithStatus(3, 4);
        Library addedLibrary = null;
        for (Library books : libraryList) {
            if (books.getBook().getId() == 3) {
                addedLibrary = books;
                break;
            }
        }
        libraryService.delete(addedLibrary);
    }

    @Test
    void addBook() throws JsonProcessingException {
        CoverService coverService = new CoverService();
        coverService.addCover("1234567890");
        List<Cover> coverList = coverService.findAll();
        coverList.sort(Comparator.comparingInt(Cover::getId));
        BookService bookService = new BookService();
        int coverId = coverList.get(coverList.size() - 1).getId();
        BookClass bookClass = new BookClass("Test Book",
                2, "test", 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), coverId, false);
        String bookJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookClass);
        int bookId = serverApplication.addBook(bookJson);
        List<Books> books = bookService.findAll();
        books.sort(Comparator.comparingInt(Books::getId));
        Books lastBook = books.get(books.size() - 1);
        assertEquals(bookId, lastBook.getId());
        assertEquals("Test Book", lastBook.getName());
        assertEquals(2, lastBook.getAuthor().getId());
        assertEquals("test", lastBook.getAnnotation());
        assertEquals(1, lastBook.getGenre().getId());
        assertEquals(1, lastBook.getCompleteness().getId());
        assertEquals(0, lastBook.getViews());
        assertEquals(Date.valueOf(LocalDate.now()).toString(), lastBook.getDate().toString());
        assertEquals(coverId, lastBook.getCover().getId());
        assertFalse(lastBook.isDraft());
        bookService.delete(lastBook);
    }

    @Test
    void addChapter() throws JsonProcessingException {

        ChaptersService chaptersService = new ChaptersService();
        BookService bookService = new BookService();
        Chapter chapter = new Chapter("TestName", "TestText");
        String chapterJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(chapter);
        int chapterId = serverApplication.addChapter(2, chapterJson);
        Chapters testChapter = chaptersService.findLastChapter(2);
        assertEquals(chapter.getName(), testChapter.getName());
        assertEquals(chapter.getText(), testChapter.getText());
        assertEquals(chapterId, testChapter.getId());
        chaptersService.deleteChapter(chaptersService.findLastChapter(2).getId());

    }

    @Test
    void changeTextInChapter() throws JsonProcessingException {
        Chapter chapter = new Chapter("Глава 1",
                "Дам, Просто умрите... Возможно в следующей жизни повезет больше.");
        String chapterJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(chapter);
        boolean isChanged = serverApplication.changeTextInChapter(1, chapterJson);
        ChaptersService chaptersService = new ChaptersService();
        Chapters chapters = chaptersService.getById(1);
        assertTrue(isChanged);
        assertEquals(chapters.getText(), chapter.getText());

        chapter.setText("Да, Просто умрите... Возможно в следующей жизни повезет больше.");
        chapterJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(chapter);
        isChanged = serverApplication.changeTextInChapter(1, chapterJson);
        chapters = chaptersService.getById(1);
        assertTrue(isChanged);
        assertEquals(chapters.getText(), chapter.getText());

    }

    @Test
    void changeBook() throws JsonProcessingException {
        BookService bookService = new BookService();
        BookClass bookClass = new BookClass("Test Book",
                2, "test", 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), 18, false);
        String bookJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookClass);
        int bookId = serverApplication.changeBook(7, bookJson);
        Books book = bookService.getById(7);
        assertEquals(bookId, 7);
        assertEquals(book.getName(), bookClass.getName());
        assertEquals(book.getAnnotation(), bookClass.getAnnotation());

        bookClass = new BookClass("Test Book",
                2, "test???", 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), 18, false);
        bookJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookClass);
        bookId = serverApplication.changeBook(7, bookJson);
        book = bookService.getById(7);
        assertEquals(bookId, 7);
        assertEquals(book.getName(), bookClass.getName());
        assertEquals(book.getAnnotation(), bookClass.getAnnotation());
    }

    @Test
    void addCover() {
        byte[] inputArray = "test String".getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile("templFile", inputArray);
        MultipartFile multipartFile = mockMultipartFile;
        int coverId = serverApplication.addCover(multipartFile);
        CoverService coverService = new CoverService();
        List<Cover> coverList = coverService.findAll();
        coverList.sort(Comparator.comparingInt(Cover::getId));
        assertEquals(coverId, coverList.get(coverList.size() - 1).getId());
        Cover cover = coverService.getById(coverId);

    }

    @Test
    void changeCover() {
        byte[] inputArray = "test String".getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile("templFile", inputArray);
        MultipartFile multipartFile = mockMultipartFile;
        boolean coverChanged = serverApplication.changeCover(79, multipartFile);
        CoverService coverService = new CoverService();
        List<Cover> coverList = coverService.findAll();
        coverList.sort(Comparator.comparingInt(Cover::getId));
        assertTrue(coverChanged);
    }

    @Test
    void search() throws JsonProcessingException {
        SearchParamsClass searchParams = new SearchParamsClass("поиск", 2, 1);
        String paramsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(searchParams);
        String librarySearch = serverApplication.search(paramsJson);
        ListBooksId listBooksId = objectMapper.readValue(librarySearch, ListBooksId.class);
        assertEquals(1, listBooksId.getBooksId().get(0));
        assertEquals(1, listBooksId.getBooksId().size());
    }

    @Test
    void librarySearch() throws JsonProcessingException {
        SearchParamsClass searchParams = new SearchParamsClass("Жизнь", 3, 1);
        String paramsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(searchParams);
        String librarySearch = serverApplication.librarySearch(paramsJson, 2);
        ListBooksId listBooksId = objectMapper.readValue(librarySearch, ListBooksId.class);
        assertEquals(3, listBooksId.getBooksId().get(0));
        assertEquals(1, listBooksId.getBooksId().size());
    }

    @Test
    void deleteChapter() {
        ChaptersService chaptersService = new ChaptersService();
        Chapter chapter = new Chapter("TestName", "TestText");
        chaptersService.addChapter(2, chapter);
        Chapters testChapter = chaptersService.findLastChapter(2);
        int id = testChapter.getId();
        boolean deleted = serverApplication.deleteChapter(id);
        assertTrue(deleted);
        assertNull(chaptersService.getById(id));
    }

    @Test
    void setDraft() {
        boolean draft = serverApplication.setDraft(2);
        assertTrue(draft);
    }

    @Test
    void getLastChapter() {
        int lastChapter = serverApplication.getLastChapter(1, 1);
        assertEquals(3, lastChapter);
    }

    @Test
    void getLastBooks() throws JsonProcessingException {
        String lastBooks = serverApplication.getLastBooks(2);
        ListBooksId listBooksId = objectMapper.readValue(lastBooks, ListBooksId.class);
        assertEquals(2, listBooksId.getBooksId().size());
        assertEquals(1, listBooksId.getBooksId().get(0));
    }

    @Test
    void getCover() throws IOException {
        CoverService coverService = new CoverService();
        Cover cover = coverService.getById(74);
        byte[] coverFile = serverApplication.getCover(74);
        assertArrayEquals(coverFile, Files.readAllBytes(Paths.get(cover.getDate())));
    }
}