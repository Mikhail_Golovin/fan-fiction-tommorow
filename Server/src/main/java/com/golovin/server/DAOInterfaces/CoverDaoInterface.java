package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface CoverDaoInterface<T, Id extends Serializable> {
    void persist(Session session, T entity);

    void update(Session session, T entity);

    T findById(Session session, Id id);

    List<T> findAll(Session session);


}
