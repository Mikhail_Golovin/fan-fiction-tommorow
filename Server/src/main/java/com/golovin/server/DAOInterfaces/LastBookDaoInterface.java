package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface LastBookDaoInterface<T, Id extends Serializable> {
    void persist(Session session, T entity);

    void update(Session session, T entity);

    void delete(Session session, T entity);

    List<T> findAll(Session session);

    List<T> findUserLastBooks(Session session, Id id);

    T findBookWithMaxPriority(Session session, Id id);

    T findBookWithMinPriority(Session session, Id id);
}
