package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface GenresDaoInterface<T, Id extends Serializable> {
    T findById(Session session, Id id);

    List<T> findAll(Session session);

}
