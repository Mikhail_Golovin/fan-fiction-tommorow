package com.golovin.server.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "обложка")
public class Cover {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name = "данные")
    private String date;

    public Cover(String date) {
        this.date = date;
    }

    public Cover() {
    }

}
