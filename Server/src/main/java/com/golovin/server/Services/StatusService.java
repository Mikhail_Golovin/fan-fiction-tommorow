package com.golovin.server.Services;

import com.golovin.server.DAO.StatusDao;
import com.golovin.server.models.Status;
import org.hibernate.Session;

public class StatusService {
    private static final StatusDao statusDao = new StatusDao();

    public StatusService() {
    }

//    public StatusService(StatusDao statusDao) {
//        this.statusDao = statusDao;
//    }

    public Status getById(int id) {
        Session session = statusDao.openCurrentSession();
        Status status = statusDao.findById(session, id);
        statusDao.closeCurrentSession(session);
        return status;
    }

    public Status test(int id) {
        Status status = getById(id);
        return status;
    }

    public StatusDao getStatusDao() {
        return statusDao;
    }

}
