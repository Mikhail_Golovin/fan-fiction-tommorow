package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.BookDaoInterface;
import com.golovin.server.models.Books;
import com.golovin.server.models.Library;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements BookDaoInterface<Books, Integer> {


    private Session currentSession;

    private Transaction currentTransaction;

    public BookDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public void closeCurrentSessionWithTransaction(Session session) {
        session.getTransaction().commit();
        session.close();
    }


    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Session session, Books book) {
        session.saveOrUpdate(book);
    }

    @Override
    public void update(Session session, Books book) {
        session.update(book);
    }

    @Override
    public void delete(Session session, Books book) {
        session.delete(book);
    }

    @Override
    public Books findById(Session session, Integer id) {
        Books book = session.get(Books.class, id);
        return book;
    }

    @Override
    public boolean isGenrePresent(Session session, Integer genreId) {
        String queryString = "from Books where genre.id = :genreid and isDraft = false";
        Query query = session.createQuery(queryString);
        query.setParameter("genreid", genreId);
        boolean isEmpty = query.list().isEmpty();

        return !isEmpty;
    }


    @Override
    public Books getUserBook(Session session, Integer userId, Integer bookId) {
        String queryString = "From Library where pk.userId = :userId and pk.bookId = :bookId";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        query.setParameter("bookId", bookId);
        if (query.list().isEmpty()) {
            return null;
        }
        Library libraryBook = (Library) query.getSingleResult();
        Books book = libraryBook.getBook();

        return book;
    }

    @Override
    public ArrayList<Books> getAuthor(Session session, Integer authorId) {
        String queryString = "from Books where author.id = :authorId order by id asc";
        Query query = session.createQuery(queryString);
        query.setParameter("authorId", authorId);
        ArrayList<Books> books = (ArrayList<Books>) query.list();

        return books;
    }

    @Override
    public ArrayList<Books> getGenre(Session session, Integer genreId) {
        String queryString = "from Books where genre.id = :genreId order by id asc";
        Query query = session.createQuery(queryString);
        query.setParameter("genreId", genreId);
        ArrayList<Books> books = (ArrayList<Books>) query.list();

        return books;
    }

    @Override
    public List<Books> getBookInSection(Session session, Integer sectionId) {
        String queryString;
        List<Books> books = new ArrayList<>();
        Query query;
        switch (sectionId) {
            case (1) -> {
                queryString = "from Books order by views desc";
                query = session.createQuery(queryString);
                if (!query.list().isEmpty()) {
                    books = (ArrayList<Books>) query.list();
                }
            }
            case (2) -> {
                queryString = "from Books where date >= :date order by views desc";
                query = session.createQuery(queryString);
                query.setParameter("date", Date.valueOf(LocalDate.now().minusMonths(2)));
                if (!query.list().isEmpty()) {
                    books = (ArrayList<Books>) query.list();
                }
            }
        }
        return books;
    }


    @Override
    public List<Books> searchBooks(Session session, String text, Integer genreId, Integer sortingType) {
        System.out.println(text + " " + genreId + " " + sortingType);
        String queryString;
        List<Books> books = new ArrayList<>();
        Query query;
        switch (sortingType) {
            case (1):
                queryString = "from Books where genre.id = : genreId and name like :text order by views desc";
                query = session.createQuery(queryString);
                query.setParameter("text", "%" + text + "%");
                query.setParameter("genreId", genreId);
                if (!query.list().isEmpty()) {
                    books = (ArrayList<Books>) query.list();
                }
                break;
            case (2):
                queryString = "from Books where genre.id = : genreId and name like :text order by date desc";
                query = session.createQuery(queryString);
                query.setParameter("text", "%" + text + "%");
                query.setParameter("genreId", genreId);
                if (!query.list().isEmpty()) {
                    books = (ArrayList<Books>) query.list();
                }
                break;
            case (3):
                queryString = "from LastBook where book.genre.id = : genreId and book.name like :text order by priority desc";
                query = session.createQuery(queryString);
                query.setParameter("text", "%" + text + "%");
                query.setParameter("genreId", genreId);
                if (!query.list().isEmpty()) {
                    books = (ArrayList<Books>) query.list();
                }
                break;
        }
        return books;
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<Books> findAll(Session session) {
        return (List<Books>) session.createQuery("FROM Books").list();
    }
}
