package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.LibraryDaoInterface;
import com.golovin.server.models.Library;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class LibraryDao implements LibraryDaoInterface<Library, Integer> {

    private Session currentSession;

    private Transaction currentTransaction;

    public LibraryDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public void closeCurrentSessionWithTransaction(Session session) {
        session.getTransaction().commit();
        session.close();
    }


    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Session session, Library library) {
        session.saveOrUpdate(library);
    }

    @Override
    public void delete(Session session, Library library) {
        session.delete(library);
    }

    @Override
    public List<Library> getBooksWithStatus(Session session, Integer statusId, Integer userId) {
        String queryString = "From Library where pk.userId = :userId and pk.statusId = :statusId";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        query.setParameter("statusId", statusId);
        List<Library> books = (List<Library>) query.list();

        return books;

    }

    @Override
    public Library getUserBookStatus(Session session, Integer userId, Integer bookId) {
        String queryString = "From Library where pk.userId = :userId and pk.bookId = :bookId";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        query.setParameter("bookId", bookId);
        Library book = null;
        if (query.list().isEmpty()) {
            return null;
        } else {
            book = (Library) query.getSingleResult();
        }
        return book;


    }

    @Override
    public List<Library> getAllBooks(Session session, Integer userId) {
        String queryString = "From Library where pk.userId = :userId";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        List<Library> books = (List<Library>) query.list();

        return books;

    }


    public Library addBookInLibrary(Session session, Integer userId, Integer statusId, Integer bookId) {
        String queryString = "From Library where pk.userId = :userId and " +
                "pk.statusId = :statusId and pk.bookId = :bookId";
        Library library = null;
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        query.setParameter("statusId", statusId);
        query.setParameter("bookId", bookId);
        String queryString2 = "From Library where pk.userId = :userId and pk.bookId = :bookId";
        Query query2 = session.createQuery(queryString2);
        query2.setParameter("userId", userId);
        query2.setParameter("bookId", bookId);
        if (query.list().isEmpty() && !query2.list().isEmpty()) {
            library = (Library) query2.getSingleResult();
            return library;
        }
        return library;

    }

    @Override
    public List<Library> searchBooks(Session session, String text, Integer genreId, Integer sortingType, Integer userId) {
        String queryString;
        List<Library> libraryList = new ArrayList<>();
        Query query;
        switch (sortingType) {
            case (1):
                queryString = "from Library where book.genre.id = : genreId " +
                        "and user.id = :userId and book.name like :text order by book.views desc";
                query = session.createQuery(queryString);
                query.setParameter("text", "%" + text + "%");
                query.setParameter("genreId", genreId);
                query.setParameter("userId", userId);
                if (!query.list().isEmpty()) {
                    libraryList = (ArrayList<Library>) query.list();
                }
                break;
            case (2):
                queryString = "from Library where book.genre.id = : genreId " +
                        "and user.id = :userId and book.name like :text order by book.date desc";
                query = session.createQuery(queryString);
                query.setParameter("text", "%" + text + "%");
                query.setParameter("genreId", genreId);
                query.setParameter("userId", userId);
                if (!query.list().isEmpty()) {
                    libraryList = (ArrayList<Library>) query.list();
                }
                break;
        }
        return libraryList;
    }


}
