package com.golovin.server.utils;

import com.golovin.server.Services.*;


public class MyCommon {

    private static final ChaptersService chapterService = new ChaptersService();
    private static final UserService userService = new UserService();
    private static final BookService bookService = new BookService();
    private static final CompletenessService completenessService = new CompletenessService();
    private static final LastBookService lastBookService = new LastBookService();
    private static final LibraryService libraryService = new LibraryService();
    private static final StatusService statusService = new StatusService();
    public static CoverService coverService = new CoverService();
    public static GenresService genresService = new GenresService();

    private MyCommon() {

    }

    public static ChaptersService getChapterService() {
        return chapterService;
    }

    public static UserService getUserService() {
        return userService;
    }

    public static BookService getBookService() {
        return bookService;
    }

    public static CompletenessService getCompletenessService() {
        return completenessService;
    }

    public static GenresService getGenresService() {
        return genresService;
    }

    public static LastBookService getLastBookService() {
        return lastBookService;
    }

    public static LibraryService getLibraryService() {
        return libraryService;
    }

    public static StatusService getStatusService() {
        return statusService;
    }

    public static CoverService getCoverService() {
        return coverService;
    }


}
